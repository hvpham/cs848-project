import psycopg2


def download():
    '''download fake users' ids'''
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost", database="ghtorrent_2015_09_25", user="user", password="pass")

        # create a cursor
        cur = conn.cursor()

        sql = """
        select pr_objects.id from pr_objects where pr_objects.table='users' and pr_objects.objectid in (select users.id from users where users.fake='true');
        """
        cur.execute(sql)

        with open('data/fake_users.csv','w') as csvfile:
            row = cur.fetchone()
            while row is not None:
                csvfile.write('%s\n' % (row[0]))
                row = cur.fetchone()

        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


download()
