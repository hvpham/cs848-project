#!/bin/bash
now="$(date +"%y_%m_%d_%H_%M_%S")"
exec &> "cal_all_rank_$now.log"

source activate allbackend

python 'DownloadLinkAndObjects.py'
python 'CalculatePageRank.py'

source deactivate
