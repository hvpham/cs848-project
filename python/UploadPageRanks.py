import psycopg2


def upload():
    '''upload page rank to database'''
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost", database="ghtorrent_2015_09_25", user="user", password="pass")

        # create a cursor
        cur = conn.cursor()

        sql = """DROP TABLE IF EXISTS "pageranks" CASCADE"""
        cur.execute(sql)

        sql = """CREATE TABLE "pageranks" (
        "id" integer primary key,
        "rank" double precision
        )"""
        cur.execute(sql)

        with open('data/pagerank.csv', 'r') as file:
            cur.copy_from(file, 'pageranks', sep=',')

        # close the communication with the PostgreSQL
        cur.close()

        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


upload()
