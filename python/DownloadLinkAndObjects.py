import psycopg2


def download():
    '''download table links and pr_objects'''
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost", database="ghtorrent_2015_09_25", user="user", password="pass")

        # create a cursor
        cur = conn.cursor()

        with open('data/links.csv', 'w') as file:
            cur.copy_to(file, 'links', sep=',')

        with open('data/pr_objects.csv', 'w') as file:
            cur.copy_to(file, 'pr_objects', sep=',')

        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


download()
