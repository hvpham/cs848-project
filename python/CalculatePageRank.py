import numpy
import csv


class Web:
    def __init__(self, n):
        self.size = n
        self.in_links = {}
        self.number_out_links = {}
        self.dangling_pages = {}
        for j in range(n):
            self.in_links[j] = []
            self.number_out_links[j] = 0
            self.dangling_pages[j] = True


def load_web(filter_list):
    '''Returns a web object with data loaded from links and pr_objects'''

    n_objects = 0
    rev_dic = dict()
    with open('data/pr_objects.csv') as csvfile:
        r = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in r:
            rev_dic[(row[1], row[2])] = int(row[0]) - 1
            n_objects = n_objects + 1

    g = Web(n_objects)

    print('Number of objects:', n_objects)

    with open('data/links.csv') as csvfile:
        r = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in r:
            if row[0] not in filter_list:
                id1 = rev_dic.get((row[1], row[2]))
                id2 = rev_dic.get((row[3], row[4]))

                if id1 is not None and id2 is not None:
                    g.in_links.get(id1).append(id2)
                    g.in_links.get(id2).append(id1)
                    g.number_out_links[id1] = g.number_out_links[id1] + 1
                    g.number_out_links[id2] = g.number_out_links[id2] + 1
                    g.dangling_pages.pop(id1, None)
                    g.dangling_pages.pop(id2, None)

    return g


def save_rank(pr, filename):
    file = open('data/' + filename, 'w')

    id = 1
    for r in pr:
        file.write('%d,%s\n' % (id, str(r)))
        id = id + 1

    file.close()


def step(g, p, s=0.85):
    '''Performs a single step in the PageRank computation,
    with web g and parameter s.  Applies the corresponding M
    matrix to the vector p, and returns the resulting
    vector.'''
    n = g.size
    v = numpy.matrix(numpy.zeros((n, 1)))
    inner_product = sum([p[j] for j in g.dangling_pages.keys()])
    for j in range(n):
        v[j] = s * sum([p[k] / g.number_out_links[k]
                        for k in g.in_links[j]]) + s * inner_product / n + (1 - s) / n
    # We rescale the return vector, so it remains a
    # probability distribution even with floating point
    # roundoff.
    return v / numpy.sum(v)


def pagerank(g, s=0.85, tolerance=0.00001):
    '''Returns the PageRank vector for the web g and
    parameter s, where the criterion for convergence is that
    we stop when M^(j+1)P-M^jP has length less than
    tolerance, in l1 norm.'''
    n = g.size
    p = numpy.matrix(numpy.ones((n, 1))) / n
    iteration = 1
    change = 2
    while change > tolerance:
        print("Iteration: %s" % iteration)
        new_p = step(g, p, s)
        change = numpy.sum(numpy.abs(p - new_p))
        print("Change in l1 norm: %s" % change)
        p = new_p
        iteration += 1

    p = numpy.array(p).reshape(-1, ).tolist()
    return p


def cal_pagerank(name, filter_list):
    print('Loading web')
    pages = load_web(filter_list)

    print('Calculate rank')
    pr = pagerank(pages, 0.85, 0.0001)

    print('Save rank')
    save_rank(pr, name)


filters = []
names = []

names.append("members_only_pagerank.csv")
filters.append(["issues-repo_id-reporter_id",
              "issue_comments-issue_id-user_id",
              "issues-repo_id-assignee_id",
              "issue_events-issue_id-actor_id",
              "commits-project_id-author_id",
              "pull_request_history-pull_request_id-actor_id",
              "commit_comments-commit_id-user_id"])

names.append("issues_only_pagerank.csv")
filters.append(["project_members-repo_id-user_id",
              "organization_members-org_id-user_id",
              "watchers-repo_id-user_id",
              "commits-project_id-author_id",
              "pull_request_history-pull_request_id-actor_id",
              "commit_comments-commit_id-user_id"])

names.append("commits_only_pagerank.csv")
filters.append(["project_members-repo_id-user_id",
              "organization_members-org_id-user_id",
              "watchers-repo_id-user_id",
              "issues-repo_id-reporter_id",
              "issue_comments-issue_id-user_id",
              "issues-repo_id-assignee_id",
              "issue_events-issue_id-actor_id"])

names.append("without_issues_pagerank.csv")
filters.append(["issues-repo_id-reporter_id",
              "issue_comments-issue_id-user_id",
              "issues-repo_id-assignee_id",
              "issue_events-issue_id-actor_id"])

names.append("without_commits_pagerank.csv")
filters.append(["commits-project_id-author_id",
              "pull_request_history-pull_request_id-actor_id",
              "commit_comments-commit_id-user_id"])

names.append("without_members_pagerank.csv")
filters.append(["project_members-repo_id-user_id",
              "organization_members-org_id-user_id",
              "watchers-repo_id-user_id"])

for i in range(len(names)):
    name = names[i]
    filter_list = filters[i]
    cal_pagerank(name, filter_list)
