import csv
import numpy as np

TOTAL_USERS = 767094
TOTAL_FAKES = 85681


def load_fake_ids():
    fake_ids = []
    with open('data/fake_users.csv') as csvfile:
        r = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in r:
            fake_ids.append(int(row[0]) - 1)
    return set(fake_ids)


def load_ranks(name):
    ranks = []
    with open('data/' + name) as csvfile:
        r = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in r:
            ranks.append(float(row[1]))
    return ranks


def cal_score(fake_ids, ranks):
    sorted_indexes = np.argsort(ranks)

    score = 0

    index = 1
    for sorted_index in sorted_indexes:
        if index > (TOTAL_USERS - TOTAL_FAKES):
            break

        if sorted_index in fake_ids:
            score = score + ((TOTAL_USERS - TOTAL_FAKES + 1) - index)

        index = index + 1

    score = score / TOTAL_FAKES

    return score


def inspect(fake_ids, name):
    print('Load rank: ', name)
    ranks = load_ranks(name)
    print('Calculating score')
    score = cal_score(fake_ids, ranks)
    print(name, ": ", str(score))


print('Load fake_ides')
fakes = load_fake_ids()

inspect(fakes, "simpleranks.csv")
inspect(fakes, "pagerank.csv")
inspect(fakes, "members_only_pagerank.csv")
inspect(fakes, "issues_only_pagerank.csv")
inspect(fakes, "commits_only_pagerank.csv")
inspect(fakes, "without_issues_pagerank.csv")
inspect(fakes, "without_commits_pagerank.csv")
inspect(fakes, "without_members_pagerank.csv")
