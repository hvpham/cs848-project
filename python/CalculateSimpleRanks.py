import psycopg2
import csv


def load_pr_object():
    rev_dic = dict()
    n_objects = 0
    with open('data/pr_objects.csv') as csvfile:
        r = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in r:
            rev_dic[(row[1], int(row[2]))] = int(row[0]) - 1
            n_objects = n_objects + 1

    return n_objects, rev_dic


def download(n_objects, rev_dic):
    ranks = [0] * n_objects

    '''calculate simple rank'''
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost", database="ghtorrent_2015_09_25", user="user", password="pass")

        # create a cursor
        cur = conn.cursor()

        sql = """
        SELECT "tableName", "idNum", SUM("count") as "link_count"
        FROM 
            (
                (SELECT "tableA" as "tableName", "idA" as "idNum", count(*) from links group by ("tableA", "idA"))
                UNION 
                (SELECT "tableB" as "tableName", "idB" as "idNum", count(*) from links group by ("tableB", "idB"))
            ) Data 
        GROUP  BY "tableName", "idNum"; 
        """
        cur.execute(sql)

        row = cur.fetchone()
        while row is not None:
            index = rev_dic[(row[0], row[1])]
            ranks[index] = int(row[2])
            row = cur.fetchone()

        # close the communication with the PostgreSQL
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print("There has been an error", error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

    return ranks


def save_rank(pr, filename):
    file = open('data/' + filename, 'w')

    id = 1
    for r in pr:
        file.write('%d,%s\n' % (id, str(r)))
        id = id + 1

    file.close()


n, dic = load_pr_object()
r = download(n, dic)
save_rank(r, 'simpleranks.csv')
