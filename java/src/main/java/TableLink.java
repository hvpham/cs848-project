public class TableLink {
    String mTableLink;
    String mALinkID;
    String mBLinkID;
    String mATable;
    String mBTable;
    String mATableID;
    String mBTableID;

    public TableLink(String tableLink, String aLinkID, String bLinkID, String aTable, String aTableID, String bTable, String bTableID) {
        mTableLink = tableLink;
        mALinkID = aLinkID;
        mBLinkID = bLinkID;
        mATable = aTable;
        mATableID = aTableID;
        mBTable = bTable;
        mBTableID = bTableID;
    }

}
