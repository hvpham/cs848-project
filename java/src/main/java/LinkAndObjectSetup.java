import java.sql.*;
import java.util.List;

public class LinkAndObjectSetup {
    public static void main(String[] args) {
        Utils.runSQLFromFile("Constants.DB", "src/main/resources/setuptableLinks.sql");
        getTheLinks();
        //Utils.runSQLFromFile("Constants.DB", "src/main/resources/setuptableObjects.sql");
    }


    private static void getTheLinks() {
        try (Connection ghTorrentCon = DriverManager.getConnection("jdbc:postgresql://deepgpu1.eng:5432/" + Constants.DB, Constants.USER, Constants.PASS)) {
            System.out.println("Connected!");

            List<TableLink> linkTableInfo = Constants.linkTableInfo;

            for (TableLink link : linkTableInfo) {
                Statement statement = ghTorrentCon.createStatement();
                String linkName = link.mTableLink+"-"+link.mALinkID+"-"+link.mBLinkID;
                String sql = "INSERT INTO links (\"linkName\", \"tableA\", \"idA\", \"tableB\", \"idB\")" +
                        " SELECT '"+ linkName + "', '" + link.mATable + "', " + link.mALinkID + ", '" + link.mBTable + "', " + link.mBLinkID +
                        " FROM " + link.mTableLink + " WHERE created_at > timestamp '2015-09-01 00:00:00' AND "+link.mALinkID+" IS NOT NULL AND "+link.mBLinkID +" IS NOT NULL";

                System.out.println(sql);
                statement.execute(sql);
                System.out.println("Done: " + link.mTableLink);
            }

        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }


}
