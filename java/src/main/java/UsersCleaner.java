import java.sql.*;

public class UsersCleaner {
    public static void main(String[] args) {

        String user_insert_sql = "update users set no_ref = ? where id = ?";
        String user_query_sql = "select user_id,count(user_id) from organization_members group by user_id order by count(*) desc";
        //getUsersNumberOfReferences(user_query_sql, user_insert_sql);

        String org_insert_sql = "update users set org_no_ref = ? where id = ?";
        String org_query_sql = "select org_id,count(org_id) from organization_members group by org_id order by count(*) desc";

        getUsersNumberOfReferences(org_query_sql, org_insert_sql);

    }

    private static void getUsersNumberOfReferences(String count_sql, String insert_sql) {
        try (
                Connection ghTorrentCon = DriverManager.getConnection("jdbc:postgresql://host:5432/ghtorrent", "user", "pass");
                Connection cs848Con = DriverManager.getConnection("jdbc:postgresql://host:5432/cs848project", "user", "pass");
        ) {
            System.out.println("Connected!");
            Statement statement = ghTorrentCon.createStatement();


            cs848Con.setAutoCommit(false);

            String insertUserSQL = insert_sql;
            PreparedStatement writeStatement = cs848Con.prepareStatement(insertUserSQL);

            int MAX_BATCH_SIZE = 10000;
            int actualbatchSize;

            int offSet = 0;

            do {
                ResultSet resultSet = statement.executeQuery(count_sql + " limit " + MAX_BATCH_SIZE + " offset " + offSet);

                actualbatchSize = 0;
                while (resultSet.next()) {
                    writeStatement.setInt(1, resultSet.getInt(2));
                    writeStatement.setInt(2, resultSet.getInt(1));

                    writeStatement.addBatch();

                    actualbatchSize++;
                }

                System.out.println("offset " + offSet + " actualsize " + actualbatchSize);

                writeStatement.executeBatch();
                cs848Con.commit();
                offSet += actualbatchSize;
            }
            while (actualbatchSize == MAX_BATCH_SIZE);

            cs848Con.setAutoCommit(true);

        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }
}
