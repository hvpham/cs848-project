import java.util.LinkedList;
import java.util.List;

public class Constants {
    public static String DB = "ghtorrent_2015_09_25";
    public static String USER = "user";
    public static String PASS = "pass";

    public static List<TableLink> linkTableInfo;

    static {

        linkTableInfo = new LinkedList<>();
        linkTableInfo.add(new TableLink(
                "organization_members", "org_id", "user_id",
                "users", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "followers", "user_id", "follower_id",
                "users", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "watchers", "repo_id", "user_id",
                "projects", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "project_members", "repo_id", "user_id",
                "projects", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "commits", "project_id", "author_id",
                "projects", "id",
                "users", "id"));
        /*
        linkTableInfo.add(new TableLink(
                "pull_requests", "base_repo_id", "head_repo_id",
                "projects", "id",
                "projects", "id"));
        linkTableInfo.add(new TableLink(
                "pull_requests", "base_commmit_id", "head_commit_id",
                "commits", "id",
                "commits", "id"));
        linkTableInfo.add(new TableLink(
                "project_commits", "project_id", "commit_id",
                "projects", "id",
                "commits", "id"));
        linkTableInfo.add(new TableLink(
                "commit_parents", "commit_id", "parent_id",
                "commits", "id",
                "commits", "id"));
         */
        linkTableInfo.add(new TableLink(
                "commit_comments", "commit_id", "user_id",
                "commits", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "issues", "repo_id", "reporter_id",
                "projects", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "issues", "repo_id", "assignee_id",
                "projects", "id",
                "users", "id"));
        /*
        linkTableInfo.add(new TableLink(
                "pull_request_commits", "pull_request_id", "commit_id",
                "pull_requests", "id",
                "commits", "id"));
         */
        linkTableInfo.add(new TableLink(
                "pull_request_history", "pull_request_id", "actor_id",
                "pull_requests", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "issue_comments", "issue_id", "user_id",
                "issues", "id",
                "users", "id"));
        linkTableInfo.add(new TableLink(
                "issue_events", "issue_id", "actor_id",
                "issues", "id",
                "users", "id"));

    }
}
