import java.io.IOException;
import java.sql.*;

import com.ibatis.common.jdbc.ScriptRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;

public class UsersCopy {
    public static void main(String[] args) {
        cleanCS848DB();
        copyUserFromGHTorrent();
    }

    private static void cleanCS848DB() {
        try (Connection cs848Con = DriverManager.getConnection("jdbc:postgresql://host:5432/cs848project", "user", "pass")) {
            String aSQLScriptFilePath = "src/main/resources/setuptableUsers.sql";

            ScriptRunner sr = new ScriptRunner(cs848Con, false, false);

            // Give the input file to Reader
            Reader reader = new BufferedReader(
                    new FileReader(aSQLScriptFilePath));

            // Exctute script
            sr.runScript(reader);
        } catch (IOException e) {
            System.out.println("Reading failure.");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }

    private static void copyUserFromGHTorrent() {
        try (
                Connection ghTorrentCon = DriverManager.getConnection("jdbc:postgresql://host:5432/ghtorrent", "user", "pass");
                Connection cs848Con = DriverManager.getConnection("jdbc:postgresql://host5432/cs848project", "user", "pass")
        ) {
            System.out.println("Connected!");
            Statement statement = ghTorrentCon.createStatement();


            cs848Con.setAutoCommit(false);

            String insertUserSQL = "insert into users"
                    + "(id, login, company, created_at, type, fake, deleted, long, lat, country_code, state, city, location) values"
                    + "(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement writeStatement = cs848Con.prepareStatement(insertUserSQL);


            int MAX_BATCH_SIZE = 10000;
            int actualbatchSize;

            int offSet = 0;

            do {
                ResultSet resultSet = statement.executeQuery("select * from users order by id limit " + MAX_BATCH_SIZE + " offset " + offSet );

                actualbatchSize = 0;
                while (resultSet.next()) {
                    writeStatement.setInt(1, resultSet.getInt(1));
                    writeStatement.setString(2, resultSet.getString(2));
                    writeStatement.setString(3, resultSet.getString(3));
                    writeStatement.setTimestamp(4, resultSet.getTimestamp(4));
                    writeStatement.setString(5, resultSet.getString(5));
                    writeStatement.setBoolean(6, resultSet.getBoolean(6));
                    writeStatement.setBoolean(7, resultSet.getBoolean(7));
                    writeStatement.setDouble(8, resultSet.getDouble(8));
                    writeStatement.setDouble(9, resultSet.getDouble(9));
                    writeStatement.setString(10, resultSet.getString(10));
                    writeStatement.setString(11, resultSet.getString(11));
                    writeStatement.setString(12, resultSet.getString(12));
                    writeStatement.setString(13, resultSet.getString(13));

                    writeStatement.addBatch();

                    actualbatchSize++;
                }

                System.out.println("offset " + offSet + " actualsize " + actualbatchSize);

                writeStatement.executeBatch();
                cs848Con.commit();
                offSet += actualbatchSize;
            }
            while (actualbatchSize == MAX_BATCH_SIZE);

            cs848Con.setAutoCommit(true);

        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }
}
