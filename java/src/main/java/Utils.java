import com.ibatis.common.jdbc.ScriptRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Utils {
    public static void runSQLFromFile(String database, String sqlFire) {
        try (Connection cs848Con = DriverManager.getConnection("jdbc:postgresql://host:5432/" + Constants.DB, Constants.USER, Constants.PASS)) {

            ScriptRunner sr = new ScriptRunner(cs848Con, false, false);

            // Give the input file to Reader
            Reader reader = new BufferedReader(
                    new FileReader(sqlFire));

            // Exctute script
            sr.runScript(reader);
        } catch (IOException e) {
            System.out.println("Reading failure.");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }
}
