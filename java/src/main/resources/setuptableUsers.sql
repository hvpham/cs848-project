SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP TABLE IF EXISTS "users" CASCADE;
CREATE TABLE "users"
(
    "id"           integer                                                      NOT NULL,
    "no_ref"       integer                     DEFAULT 0,
    "org_no_ref"   integer                     DEFAULT 0,
    "login"        character varying(255)                                       NOT NULL,
    "company"      character varying(255),
    "created_at"   timestamp without time zone DEFAULT CURRENT_TIMESTAMP        NOT NULL,
    "type"         character varying(255)      DEFAULT 'USR'::character varying NOT NULL,
    "fake"         boolean                     DEFAULT false                    NOT NULL,
    "deleted"      boolean                     DEFAULT false                    NOT NULL,
    "long"         numeric(11, 8),
    "lat"          numeric(10, 8),
    "country_code" character(3),
    "state"        character varying(255),
    "city"         character varying(255),
    "location"     text
)
    WITHOUT OIDS;

ALTER TABLE "users"
    ADD CONSTRAINT "users_id_pkey" PRIMARY KEY (id);