SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP TABLE IF EXISTS "pr_objects" CASCADE;
CREATE TABLE "pr_objects"
(
    "id"       SERIAL PRIMARY KEY,
    "table"    character varying(255) NOT NULL,
    "objectid" integer                NOT NULL,
    "pr"       double precision
);

INSERT INTO pr_objects ("table", "objectid")
    (SELECT "tableA" AS "table", "idA" AS "objectid"
     from links
     UNION
     SELECT "tableB", "idB"
     FROM links);
