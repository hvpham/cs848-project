SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP TABLE IF EXISTS "links" CASCADE;
CREATE TABLE "links"
(
    "linkName" character varying(255) NOT NULL,
    "tableA" character varying(255) NOT NULL,
    "idA"    integer                NOT NULL,
    "tableB" character varying(255) NOT NULL,
    "idB"    integer                NOT NULL
)
    WITHOUT OIDS;